export default {
  install: Vue => {
    Vue.prototype.evenOdd = n => {
      return parseInt(n) % 2 === 0 ? 'even' : 'odd'
    }
  }
}
