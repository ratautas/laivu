export default {
  install: Vue => {
    Vue.prototype.splitText = text => {
      return text.replace(
        /([^\x00-\x80]|\w)/g,
        '<span>$&</span>'
      )
    }
  }
}
