export default {
  install: Vue => {
    Vue.prototype.bg = url => {
      return { 'background-image': 'url(' + url + ')' }
    }
  }
}
