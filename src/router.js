import Vue from 'vue'
import Router from 'vue-router'
import NotFoundView from '@/views/NotFound'
import HomeView from '@/views/Home'
import AboutView from '@/views/About'
import SupportView from '@/views/Support'
// import NewsView from '@/views/News'
// import GalleryView from '@/views/Gallery'

Vue.use(Router)

export default new Router({
  mode: 'history',
  linkActiveClass: 'is-active',
  linkExactActiveClass: 'is-exact',
  routes: [
    {
      path: '*',
      name: 'NotFound',
      component: NotFoundView
    },
    {
      path: '/',
      name: 'Home',
      component: HomeView
    },
    {
      path: '/laivas',
      name: 'About',
      component: AboutView
    },
    {
      path: '/reikejimai',
      name: 'Support',
      component: SupportView
    // },
    // {
    //   path: '/naujienos',
    //   name: 'News',
    //   component: NewsView
    // },
    // {
    //   path: '/nuotraukos',
    //   name: 'Gallery',
    //   component: GalleryView
    }
  ]
})
