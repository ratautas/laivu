import { mapGetters, mapActions } from 'vuex'
// import ifDefined from '@/tools/ifDefined'

export default {
  data() {
    return {}
  },
  computed: {
    ...mapGetters(['baseURL', 'ui', 'page', 'pageMeta'])
  },
  metaInfo() {
    return {
      title: 'KEBNEKAISE',
      meta: [
        { property: 'og:type', content: 'website' },
        { name: 'description', content: 'KEBNEKAISE - tikrų tikriausias savadarbis laivas su kapitono tilteliu ir romo sandėliais triume. ' },
        { property: 'og:title', content: 'KEBNEKAISE' },
        { property: 'og:description', content: 'KEBNEKAISE - tikrų tikriausias savadarbis laivas su kapitono tilteliu ir romo sandėliais triume. ' },
        { property: 'og:url', content: 'https://laivu.lt/' + this.page.path },
        { property: 'og:image', content: 'https://laivu.lt/img/hero.acd68c07.jpg' }
      ]
    }
  },
  updated() { },
  methods: {
    ...mapActions([])
    // ...mapActions(['setCurrentPage']),
  },
  mounted() { }
}
