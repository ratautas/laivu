import Vue from 'vue'
import Meta from 'vue-meta'

import 'es6-promise/auto'
import './registerServiceWorker'

import App from '@/App'
import router from '@/router'
import VueAnalytics from 'vue-analytics'

import bg from '@/plugins/bg'
import splitText from '@/plugins/splitText'
import evenOdd from '@/plugins/evenOdd'

Vue.use(Meta, {
  attribute: 'data-meta',
  ssrAttribute: 'data-ssr-meta'
})

Vue.use(VueAnalytics, {
  router,
  id: 'UA-117494432-1',
  checkDuplicatedScript: true
})

Vue.use(bg)
Vue.use(splitText)
Vue.use(evenOdd)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
