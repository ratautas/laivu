var ifDefined = d => {
  if (typeof d !== 'undefined') {
    return d
  } else {
    return false
  }
}

export default ifDefined
