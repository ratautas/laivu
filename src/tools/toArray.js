// converts Object to Array and saves additional key value to inner Object
//
// usage:
// var newObject = sortBy(object, assignedKey)
import trim from '@/tools/trim'

const toArray = (object, key) => {
  var array = []
  Object.keys(object).forEach(e => {
    var newObject = trim(object[e])
    if (key) {
      newObject[key] = e
    }
    array.push(newObject)
  })
  return array
}

export default toArray
