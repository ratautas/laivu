var evenOdd = n => {
  return parseInt(n) % 2 === 0 ? 'even' : 'odd'
}

export default evenOdd
