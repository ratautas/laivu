// converts arrays like [1,2,3,4,5] into arrays of arrays [[1,2], [3,4], [5]].

// usage:
// var chunked = chunk(array, size)

const chunk = (a, s) => {
  var r = []
  s = parseInt(s) || 2 // default
  for (var x = 0; x < Math.ceil(a.length / s); x++) {
    var start = x * s
    var end = start + s
    r.push(a.slice(start, end))
  }
  return r
}

export default chunk
