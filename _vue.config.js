const path = require('path')
const PrerenderSPAPlugin = require('prerender-spa-plugin')
const GoogleFontsPlugin = require('google-fonts-plugin').default
const ExtractTextPlugin = require('extract-text-webpack-plugin')

module.exports = {
  lintOnSave: false,
  chainWebpack: webpackConfig => {
    if (process.env.NODE_ENV === 'production') {
      const inlineLimit = 10000
      const assetsPath = ''

      webpackConfig
        .output
        .filename(path.join(assetsPath, 'js/[name].js'))
        .chunkFilename(path.join(assetsPath, 'js/chunk[id].js'))

      webpackConfig.plugin('extract-css')
        .use(ExtractTextPlugin, [{
          filename: path.join(assetsPath, 'css/[name].css'),
          allChunks: true,
          disable: true
        }])

      webpackConfig.module
        .rule('images')
        .test(/\.(png|jpe?g|gif)(\?.*)?$/)
        .use('url-loader')
        .loader('url-loader')
        .options({
          limit: inlineLimit,
          name: path.join(assetsPath, 'img/[name].[ext]')
        })

      // webpackConfig.module
      //   .rule('fonts')
      //   .test(/\.(woff2?|eot|ttf|otf)(\?.*)?$/i)
      //   .use('url-loader')
      //   .loader('url-loader')
      //   .options({
      //     limit: inlineLimit,
      //     name: path.join(assetsPath, 'fonts/[name].[hash:8].[ext]')
      //   })
    }
  },
  configureWebpack: {
    plugins: [
      new PrerenderSPAPlugin({
        // Required - The path to the webpack-outputted app to prerender.
        staticDir: path.join(__dirname, 'dist'),
        // Required - Routes to render.
        routes: ['/']
      }),
      new GoogleFontsPlugin({
        'fonts': [
          {
            'family': 'Source+Sans+Pro',
            'variants': [
              '300',
              '300i'
            ],
            'subsets': [
              'latin-ext'
            ]
          },
          {
            'family': 'Yrsa',
            'variants': [
              '600'
            ],
            'subsets': [
              'latin-ext'
            ]
          }
        ],
        'outputDir': 'src/assets/font64'
      })
    ]
  }
}
