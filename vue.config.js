const path = require('path')
const PrerenderSPAPlugin = require('prerender-spa-plugin')
const GoogleFontsPlugin = require('google-fonts-plugin').default
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const FaviconsWebpackPlugin = require('favicons-webpack-plugin')

const enabled = false

module.exports = {
  lintOnSave: false,
  chainWebpack: webpackConfig => {
    if (process.env.NODE_ENV === 'production' && enabled) {
      const inlineLimit = 10000
      const assetsPath = ''

      webpackConfig
        .output
        .filename(path.join(assetsPath, 'js/[name].js'))
        .chunkFilename(path.join(assetsPath, 'js/chunk[id].js'))

      webpackConfig.plugin('extract-css')
        .use(ExtractTextPlugin, [{
          filename: path.join(assetsPath, 'css/[name].css'),
          allChunks: true,
          disable: true
        }])

      webpackConfig.module
        .rule('images')
        .test(/\.(png|jpe?g|gif)(\?.*)?$/)
        .use('url-loader')
        .loader('url-loader')
        .options({
          limit: inlineLimit,
          name: path.join(assetsPath, 'img/[name].[ext]')
        })

      // webpackConfig.module
      //   .rule('fonts')
      //   .test(/\.(woff2?|eot|ttf|otf)(\?.*)?$/i)
      //   .use('url-loader')
      //   .loader('url-loader')
      //   .options({
      //     limit: inlineLimit,
      //     name: path.join(assetsPath, 'fonts/[name].[hash:8].[ext]')
      //   })
    }
  },
  configureWebpack: {
    plugins: [
      new PrerenderSPAPlugin({
        // Required - The path to the webpack-outputted app to prerender.
        staticDir: path.join(__dirname, 'dist'),
        // Required - Routes to render.
        routes: ['/', '/laivas', '/reikejimai']
      }),
      new GoogleFontsPlugin({
        'fonts': [
          {
            'family': 'Raleway',
            'variants': [
              '300',
              '300i',
              '700',
              '700i'
            ],
            'subsets': [
              'latin-ext'
            ]
          },
          {
            'family': 'Montserrat',
            'variants': [
              '700',
              '900'
            ],
            'subsets': [
              'latin-ext'
            ]
          }
        ],
        'outputDir': 'src/assets/fonts64'
      }),
      new FaviconsWebpackPlugin({
        // Your source logo
        logo: './src/assets/img/favicon.png',
        // The prefix for all image files (might be a folder or a name)
        prefix: 'img/icons/',
        // Emit all stats of the generated icons
        emitStats: false,
        // The name of the json containing all favicon information
        // statsFilename: 'iconstats-[hash].json',
        // Generate a cache file with control hashes and
        // don't rebuild the favicons until those hashes change
        persistentCache: true,
        // Inject the html into the html-webpack-plugin
        inject: true,
        // favicon background color (see https://github.com/haydenbleasel/favicons#usage)
        // background: '#fff',
        // favicon app title (see https://github.com/haydenbleasel/favicons#usage)
        title: 'KEBNEKAISE'
      })
    ]
  }
}
